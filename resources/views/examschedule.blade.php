@extends('templates.template')
@section('title','Schedule')
@section('content')

<h1 class="text-center py-5">Examination Schedule</h1>
	@if(Session::has("message"))
		<h4 class="text-center alert alert-success">{{Session::get('message')}}</h4>
	@endif
	

<!-- ADD SCHEDULE MODAL -->
<div class="modal fade" id="addschedule">
		  <div class="modal-dialog " role="document">
		    <div class="modal-content" style="bg-color: green;">
		      <div class="modal-header">
		        <h5 class="modal-title">Add New Schedule</h5>

		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		        <form action="/examschedule" method="POST">
				@csrf				
			      <div class="modal-body">
					<div class="form-group">
						<label>Date of Examination:</label>
						<input type="date" name="date" class="form-control">
					</div>				
					<div class="form-group">
						<label for="subject">Subject:</label>
						<select name="subject_id" class="form-control">
							@foreach($subjects as $subject)
							<option value="{{$subject->id}}">{{$subject->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="batch">Batch:</label>
						<select name="batch_id" class="form-control">
							@foreach($batches as $batch)
								<option value="{{$batch->id}}">{{$batch->name}}</option>
							@endforeach
						</select>
					</div>	
					<div class="form-group">
						<label>Room No:</label>
						<input type="number" name="roomNo" class="form-control">
					</div>
					<div class="form-group">
						<label>Start Time:</label>
						<input type="time" name="startTime" class="form-control">
					</div>
					<div class="form-group">
						<label>End Time:</label>
						<input type="time" name="endTime" class="form-control">
					</div>
					<button type="submit" class="btn btn-success">Add Schedule</button>			
			      </div>
			      <!-- <div class="offset-lg-1"> -->
					<!-- <a data-toggle="modal" data-target="#addstudent" class="btn btn-primary">Add New </a>	 -->
				<!-- </div> -->
				</form>
		    </div>
		 </div>
</div>
<!-- END ADD SCHEDULE MODAL -->


<!-- EDIT SCHEDULE MODAL -->
	<div class="modal fade" id="editschedule">
		  <div class="modal-dialog " role="document">
		    <div class="modal-content" style="bg-color: green;">
		      <div class="modal-header">
		        <h5 class="modal-title">Edit Schedule</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hiddens="true">&times;</span>
		        </button>
		      </div>
		      <!-- @foreach($exam_scheds as $exam_sched) -->
				<form action="/examschedule/{{$exam_sched->id}}" method="POST">
				@csrf				
			      <div class="modal-body">
					@csrf
					{{method_field("PATCH")}}
					<div class="form-group">
						<label for="date">Date of Examination:</label>
						<input type="date" name="date" class="form-control" value="{{$exam_sched->date}}">
					</div>				
					<div class="form-group">
						<label for="subject_id">Subject:</label>
						<select name="subject_id" class="form-control" >
							@foreach($subjects as $subject)
							<option value="{{$subject->id}}" {{$subject->id == $exam_sched->subject_id ? "selected" : ""}}>{{$subject->name}}</option>
							@endforeach
						</select>					
					</div>
						<div class="form-group">
						<label for="batch">Batch:</label>
						<select name="batch_id" class="form-control">
							@foreach($batches as $batch)
								<option value="{{$batch->id}}">{{$batch->name}}</option>
							@endforeach
						</select>
					</div>	
					<div class="form-group">
						<label for="roomNo">Room Number:</label>
						<input type="number" name="roomNo" class="form-control" value="{{$exam_sched->roomNo}}">
					</div>
					<div class="form-group">
						<label for="endTime">Start Time:</label>
						<input type="time" name="startTime" class="form-control" value="{{$exam_sched->endTime}}">
					</div>
					<div class="form-group">
						<label for="endTime">End Time:</label>
						<input type="time" name="endTime" class="form-control" value="{{$exam_sched->endTime}}">
					</div>
					
					<button type="submit" class="btn btn-success">Update</button>
				</div>
				</form>
		      <!-- @endforeach	        -->
			</div>

		</div>
	</div>

<!-- END EDIT SCHEDULE MODAL -->
	

<div class="container">
	@auth
	@if(Auth::user()->role_id == 1)
	<div class="offset-lg-1">
		<a data-toggle="modal" data-target="#addschedule" class="btn btn-primary"><i class="far fa-calendar-plus"></i> Schedule</a>			
	</div>
	@endauth
	@endif

	<div class="row">
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped ">
				<thead>
					<tr>
						<th>Examination Date</th>
						<th>Batch</th>
						<th>Subject</th>
						<th>Room No</th>
						<th>Start Time</th>
						<th>End Time</th>
						@auth
							@if(Auth::user()->role_id == 1)
						<th>Action</th>						
							@endif
							@endauth
					</tr>
				</thead>
				<tbody>
					@foreach($exam_scheds as $exam_sched)
					<tr>
						<td>{{$exam_sched->date}}</td>
						<td>{{$exam_sched->batch->name}}</td>
						<td>{{$exam_sched->subject->name}}</td>
						<td>{{$exam_sched->roomNo}}</td>						
						<td>{{$exam_sched->startTime}}</td>
						<td>{{$exam_sched->endTime}}</td>						
						<td>
							@auth
							@if(Auth::user()->role_id == 1)
							<button type="submit" class="btn btn-info form-control" data-toggle="modal" data-target="#editschedule"><i class="fa fa-edit" style="font-size:20px;color:#fff;"></i></button>
							<form class="delete_form" action="/examschedule/{{$exam_sched->id}}" method="POST">
								@csrf
								{{method_field("DELETE")}}
								<input type="hidden" name="_method" value="DELETE">
								<button type="submit" class="btn btn-danger form-control"><i class="fa fa-remove" style="font-size:20px;color:#fff;"></i></button>
							</form>
							@endif
							@endauth
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>		
			
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.delete_form').on('submit',function(){
			if(confirm("Are you sure you want to delete it?"))
			{
				return true;
			}else{
				return false;
			}
		});
	});
</script>
@endsection

