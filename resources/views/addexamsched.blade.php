@extends('templates.template')
@section('title','Add Exam Schedule')
@section('content')
<h1 class="text-center py-5">Exam Schedule</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-6 offset-lg-3">
			<form action="/addexamsched" method="POST">
				@csrf				
				<div class="form-group">
					<label>Date of Examination:</label>
					<input type="date" name="date" class="form-control">
				</div>				
				<div class="form-group">
					<label>Start Time:</label>
					<input type="time" name="startTime" class="form-control">
				</div>
				<div class="form-group">
					<label>End Time:</label>
					<input type="time" name="endTime" class="form-control">
				</div>
				<div class="form-group">
					<label>Room No:</label>
					<input type="number" name="roomNo" class="form-control">
				</div>
				<div class="form-group">
					<label for="subject">Subject:</label>
					<select name="subject_id" class="form-control">
						@foreach($subjects as $subject)
						<option value="{{$subject->id}}">{{$subject->name}}</option>
						@endforeach
					</select>
				</div>	
				<button type="submit" class="btn btn-success">Add Schedule</button>			
			</form>
		</div>
	</div>
</div>
@endsection