<html>
<head>
    <meta charset="UTF-8">
    <title>@yield("title")</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/sandstone/bootstrap.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- font awesome -->
    <script src="https://kit.fontawesome.com/31bc0cb6ea.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   
     <!-- Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .signup-button{
                width: 170px;
                height: 40px;
                margin:auto;
                margin-top: 50px;
                border: 1px solid #00a8ff;

            }
            .signup-button:hover{
                border: 2px solid #00a8ff;
                color:white;
                box-shadow: 2px 2px 16px 0px rgba(0,0,0,0.75);
                background-color:#00a8ff;

            }

            .signup-button a{
                color: #fff;
                font-size:20px;

                
            }
            .signup-button a:hover{
                text-decoration: none;
                color: #fff;
                font-size:20px;


                
            }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }
            .banner{
                background-image: url(images/bg.jpg);
                background-size:cover; 
            }

            .title {
                font-size: 65px;
                color:#00a8ff;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="/">ORS</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarColor02">
        <ul class="navbar-nav ml-auto">

        @if(Auth::user())
            <li class="nav-item">
                <a class="nav-link" href="/" ><span><i class="fa fa-home"></i></span>Home</a>
            </li>
            
           <li class="nav-item">
                <a class="nav-link" href="/examschedule"><i class='far fa-calendar-alt'></i> Schedules</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/student"><i class="fa fa-pencil-square-o"></i> Records</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/studentrequest">Request</a>
            </li>
             <li class="nav-item">
                    <a class="btn btn-primary" href=""><i class="fa fa-user-circle" aria-hidden="true"></i> Hi! {{Auth::user()->name}}</a>
                  </li>  
            <li class="nav-item">
                <form action="/logout" method="POST">
                    @csrf
                    <button class="btn btn-primary">Logout</button>
                  </form>
            </li>
        @else

            <li class="nav-item">
                <a class="nav-link" href="/register">Register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/login">Login</a>
            </li>
        
        @endif




        </ul>
      </div>
    </nav>

        @yield("content")

    <!-- <footer class="footer bg-primary">
        <div class="container">
            <p class="text-center text-white">capstone 2</p>
        </div>
    </footer> -->


    <!-- bootstrap -->
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>