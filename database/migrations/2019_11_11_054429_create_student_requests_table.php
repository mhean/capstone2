<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email');
            $table->string('studentRequest');
            $table->unsignedBigInteger('student_id');
            $table->unsignedBigInteger('batch_id');
            $table->unsignedBigInteger('subject_id');

            $table->timestamps();
            $table->foreign('student_id')
            ->on('students')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('batch_id')
            ->on('batches')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->foreign('subject_id')
            ->on('subjects')
            ->references('id')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_requests');
    }
}
