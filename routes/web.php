<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// adding student
Route::get('/student', 'StudentController@index')->name('student');
//filtering
Route::post('/student/filter','StudentController@index');

Route::get('/student', 'StudentController@create');
Route::post('/student','StudentController@store');
// edit student
Route::get('/admin/editstudent/{id}', 'StudentController@edit');
Route::patch('/admin/editstudent/{id}','StudentController@update');
// remove/deleting 
Route::delete('/admin/removestudent/{id}','StudentController@delete');



//EXAM SCHEDULE
	//adding
	Route::get('/examschedule', 'ExamSchedController@index');
	Route::get('/examschedule', 'ExamSchedController@create');
	Route::post('/examschedule', 'ExamSchedController@store');
	//editing
	Route::get('/examschedule/{id}', 'ExamSchedController@edit');
	Route::patch('/examschedule/{id}', 'ExamSchedController@update');
	//deleting
	Route::delete('/examschedule/{id}', 'ExamSchedController@delete');

//USER CONTROLLER
	Route::get('/users','UserController@index');


//STUDENT REQUEST CONTROLLER//
	//create CRUD
	Route::get('/studentrequest', 'StudentRequestController@index');
	Route::get('/studentrequest', 'StudentRequestController@create');
	Route::post('/studentrequest', 'StudentRequestController@store');

	Route::delete('/studentrequest/delete/{id}', 'StudentRequestController@delete');


