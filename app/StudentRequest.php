<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentRequest extends Model
{
	public function student(){
      return $this->belongsTo('\App\Student','student_id');
    }
    public function subject(){
      return $this->belongsTo('\App\Subject','subject_id');
    }
    public function batch(){
      return $this->belongsTo('\App\Batch','batch_id');
    }
}
