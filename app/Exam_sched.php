<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam_sched extends Model
{
     public function subject(){
      return $this->belongsTo('\App\Subject','subject_id');
    }
    public function batch(){
      return $this->belongsTo('\App\Batch','batch_id');
    }
}