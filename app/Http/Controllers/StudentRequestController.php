<?php

namespace App\Http\Controllers;

use App\StudentRequest;
use App\Student;
use App\Batch;
use App\Subject;
use Session;

use Illuminate\Http\Request;

class StudentRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('requestform');    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $student_requests = StudentRequest::all();
        $batches=Batch::all();
        $subjects=Subject::all();
        $students=Student::all();
        return view('requestform',compact('student_requests','batches','subjects','students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=array(
            "student_id"=>"required",
            "batch_id"=>"required",
            "subject_id"=>"required",
            "email"=>"required",
            "request"=>"required"            
        );

        $this->validate($request, $rules);

        $student_request = new StudentRequest;
        $student_request->student_id=$request->student_id;       
        $student_request->batch_id = $request->batch_id;      
        $student_request->subject_id = $request->subject_id;
        $student_request->email = $request->email;
        $student_request->request = $request->request;
       
        $student_request->save();

        Session::flash("message","New record has been added!");

        return redirect('/studentrequest');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\StudentRequest  $studentRequest
     * @return \Illuminate\Http\Response
     */
    public function show(StudentRequest $studentRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentRequest  $studentRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentRequest $studentRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentRequest  $studentRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentRequest $studentRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentRequest  $studentRequest
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $student_requests = StudentRequest::find($id);
        $student_requestToRemove=StudentRequest::find($id);
        $student_requestToRemove->delete(); 
        Session::flash("message","$student_request->name succesfully deleted!");
        return redirect('/studentrequest');


    }
}
