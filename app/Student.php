<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function batch(){
    	return $this->belongsTo('\App\Batch','batch_id');
    }
     public function subject(){
    	return $this->belongsTo('\App\Subject','subject_id');
    }
     public function mark(){
    	return $this->belongsTo('\App\Mark','mark_id');
    }
    public function role(){
    	return $this->belongsTo('\App\Role','role_id');
	}
}
